#ifndef PERSONREPOSITORY_H
#define PERSONREPOSITORY_H

#include "person.h"
#include "settings.h"
#include <QtSql>
#include <string>

class PersonRepository {
public:
    PersonRepository();
    // destructor used to remove the connection to the database
    ~PersonRepository();
    // adds a person to the database
    void add(const Person& p);
    // adds a row to the owners table indicating a connection between a person and a computer
    bool connect(int personId, int computerId);
    // makes a query from the database and returns vector of persons in a specific order, returns true if successful
    vector<Person> sort(const string& columnName, const string& order);
    // makes a query from the database and returns vector of persons that include the searchString
    vector<Person> find(const string& searchString);
    // makes a query from the database and returns vector of persons that are connected to the computerId
    vector<Person> getConnectedPersons(int computerId);
    // marks the Deleted column with true (1) for the person with ID personId
    void removePerson(int personId);
    // creates the desired tables in the database
    void createDatabase();
private:
    QSqlDatabase db;
    // opens the database if it's not already open
    void open();
    // closes the database if it is open
    void close();
    // builds a vector from a query
    vector<Person> buildVector(QSqlQuery &query);
};

#endif // PERSONREPOSITORY_H
