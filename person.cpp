#include "person.h"

Person::Person() {
    id = 0;
    name = "";
    birthYear = "";
    deathYear = "";
    gender = "";
}

Person::Person(const string& myName, const string& myBirthYear, const string& myDeathyear, const string& myGender, const string& myImagePath) {
    id = 0;
    name = myName;
    birthYear = myBirthYear;
    deathYear = myDeathyear;
    gender = myGender;
    imagePath = myImagePath;
}

Person:: Person(int myId, const string& myName, const string& myBirthYear, const string& myDeathyear, const string& myGender, const string& myImagePath) {
    id = myId;
    name = myName;
    birthYear = myBirthYear;
    deathYear = myDeathyear;
    gender = myGender;
    imagePath = myImagePath;
}

string Person::getName() const {
    return name;
}

string Person::getBirthyear() const {
    return birthYear;
}

string Person::getDeathyear() const {
    return deathYear;
}

string Person::getGender() const {
    return gender;
}

string Person::getImagePath() const {
    return imagePath;
}

int Person::getId() const {
    return id;
}
