#ifndef ADDPERSONDIALOG_H
#define ADDPERSONDIALOG_H

#include <QDialog>
#include <QDate>
#include <QFileDialog>
#include "service.h"
#include "person.h"

namespace Ui {
class AddPersonDialog;
}

class AddPersonDialog : public QDialog {
    Q_OBJECT

public:
    explicit AddPersonDialog(QWidget *parent = 0);
    ~AddPersonDialog();

private slots:
    void on_addPersonButton_clicked();
    void on_aliveCheckBox_stateChanged(int checked);
    void on_browseButton_clicked();

private:
    Service service;
    Ui::AddPersonDialog *ui;
    // checks if the all fields are correctly filled out
    bool validPerson();
};

#endif // ADDPERSONDIALOG_H
