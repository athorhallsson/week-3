#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow) {
    ui->setupUi(this);
    setInitialVariables();
    updatePersons();
    displayCurrentPersons();
    updateComputers();
    displayCurrentComputers();
}

MainWindow::~MainWindow() {
    delete ui;
}

void MainWindow::on_searchField_textChanged(const QString &searchString) {
    if (ui->searchField->text().isEmpty()) {   // if the seachfield is empty then sort by currentSortingMethod
        updatePersons();
        updateComputers();
    }
    else {
        currentPersons = service.findPerson(searchString.toStdString());
        currentComputers = service.findComputer(searchString.toStdString());
    }
    displayCurrentPersons();
    displayCurrentComputers();
}

void MainWindow::displayCurrentPersons() {
    ui->personTableWidget->setRowCount(currentPersons.size());
    ui->personTableWidget->clearContents();
    for (unsigned int i = 0; i < currentPersons.size(); i++) {
        ui->personTableWidget->setItem(i, 0, new QTableWidgetItem(QString::fromStdString(currentPersons[i].getName())));
        ui->personTableWidget->setItem(i, 1, new QTableWidgetItem(QString::fromStdString(currentPersons[i].getBirthyear())));
        ui->personTableWidget->setItem(i, 2, new QTableWidgetItem(QString::fromStdString(currentPersons[i].getDeathyear())));
        ui->personTableWidget->setItem(i, 3, new QTableWidgetItem(QString::fromStdString(currentPersons[i].getGender())));
    }
}

void MainWindow::displayCurrentComputers() {
    ui->computerTableWidget->setRowCount(currentComputers.size());
    ui->computerTableWidget->clearContents();
    for (unsigned int i = 0; i < currentComputers.size(); i++) {
        ui->computerTableWidget->setItem(i, 0, new QTableWidgetItem(QString::fromStdString(currentComputers[i].getName())));
        ui->computerTableWidget->setItem(i, 1, new QTableWidgetItem(QString::fromStdString(currentComputers[i].getYearBuilt())));
        ui->computerTableWidget->setItem(i, 2, new QTableWidgetItem(QString::fromStdString(currentComputers[i].getType())));
    }
}

void MainWindow::makePersonTable() {
    personHeaderView = new QHeaderView(Qt::Horizontal);
    ui->personTableWidget->setHorizontalHeader(personHeaderView);
    ui->personTableWidget->setHorizontalHeaderLabels(QString("Name;Year of birth;Year of death;Gender").split(";"));
    personHeaderView->setSectionsClickable(true);
    connect(personHeaderView,SIGNAL(sectionClicked(int)),this,SLOT(personSectionClickedSlot(int)));     // make a new slot for sorting
    ui->personTableWidget->setColumnWidth(0, 250);                                                      // set the width of the name colum to 250px
    ui->personTableWidget->horizontalHeader()->setSectionResizeMode(3, QHeaderView::Stretch);
}

void MainWindow::makeComputerTable() {
    computerHeaderView = new QHeaderView(Qt::Horizontal);
    ui->computerTableWidget->setHorizontalHeader(computerHeaderView);
    ui->computerTableWidget->setHorizontalHeaderLabels(QString("Name;Year built;Type").split(";"));
    computerHeaderView->setSectionsClickable(true);
    connect(computerHeaderView,SIGNAL(sectionClicked(int)),this,SLOT(computerSectionClickedSlot(int))); // make a new slot for sorting
    ui->computerTableWidget->setColumnWidth(0, 250);                                                    // set the width of the name colum to 250px
    ui->computerTableWidget->horizontalHeader()->setSectionResizeMode(2, QHeaderView::Stretch);
}

void MainWindow::on_addNewButton_clicked() {
    ui->errorLabel->clear();
    if (ui->tabWidget->currentIndex() == 0) {       // if the personTab was selected
        AddPersonDialog addPersonDialog;
        addPersonDialog.exec();
        updatePersons();
        displayCurrentPersons();
    }
    if (ui->tabWidget->currentIndex() == 1) {       // if the computerTab was selected
        AddComputerDialog addComputerDialog;
        addComputerDialog.exec();
        updateComputers();
        displayCurrentComputers();
    }
}

void MainWindow::on_removeButton_clicked() {
    ui->errorLabel->clear();
    if (!nothingSelected()) {                           // if there is something selected
        if (ui->tabWidget->currentIndex() == 0) {
            service.removePerson(currentPersons[ui->personTableWidget->currentRow()].getId());
            updatePersons();
            displayCurrentPersons();
        }
        if (ui->tabWidget->currentIndex() == 1) {
            service.removeComputer(currentComputers[ui->computerTableWidget->currentRow()].getId());
            updateComputers();
            displayCurrentComputers();
        }
    }
}

void MainWindow::on_connectButton_clicked() {
    ui->errorLabel->clear();
    if (!nothingSelected()) {                                                            // if there is something selected
        if (ui->tabWidget->currentIndex() == 0) {
            personId = currentPersons[ui->personTableWidget->currentRow()].getId();     // set personId to the selected person's ID
            ui->tabWidget->setCurrentIndex(1);                                          // switch tabs
            ui->errorLabel->setText("Select and press connect");
        }
        if (ui->tabWidget->currentIndex() == 1) {
            computerId = currentComputers[ui->computerTableWidget->currentRow()].getId(); // set computerId to the selected computer's ID
            ui->tabWidget->setCurrentIndex(0);                                          // switch tabs
            ui->errorLabel->setText("Select and press connect");
        }
        if (personId != 0 && computerId != 0) {                                         // if both IDs are not 0 then connect them
            if (service.connect(personId, computerId)) {
                ui->errorLabel->setText("Connection successful");
            }
            else {
                ui->errorLabel->setText("Already connected");
            }
            personId = 0;           // reset the IDs
            computerId = 0;
        }
    }
}

void MainWindow::on_infoButton_clicked() {
    ui->errorLabel->clear();
    if (!nothingSelected()) {                               // if there is something selected
        if (ui->tabWidget->currentIndex() == 0) {
            PersonInfoDialog personInfoDialog;
            personInfoDialog.setPerson(currentPersons[ui->personTableWidget->currentRow()]);    // display info for the selected person
            personInfoDialog.exec();
        }
        if (ui->tabWidget->currentIndex() == 1) {
            ComputerInfoDialog computerInfoDialog;
            computerInfoDialog.setComputer(currentComputers[ui->computerTableWidget->currentRow()]); // display info for the selected computer
            computerInfoDialog.exec();
        }
    }
}

void MainWindow::computerSectionClickedSlot(int index) {
    if (index == 0) {                                                                   // if the name headercolumn was clicked
        if (currentComputerSortingMethod == "Name" && currentComputerOrder == "ASC") {  // if already sorting by name and ascending
            currentComputers = service.sortComputers("Name", "DESC");                   // change the order to descending
            currentComputerOrder = "DESC";
        }
        else {
            currentComputers = service.sortComputers("Name", "ASC");                    // otherwise sort ascending
            currentComputerSortingMethod = "Name";                                      // update the strings to current values
            currentComputerOrder = "ASC";
        }
    }
    if (index == 1) {
        if (currentComputerSortingMethod == "Yearbuilt" && currentComputerOrder == "ASC") {
            currentComputers = service.sortComputers("Yearbuilt", "DESC");
            currentComputerOrder = "DESC";
        }
        else {
            currentComputers = service.sortComputers("Yearbuilt", "ASC");
            currentComputerSortingMethod = "Yearbuilt";
            currentComputerOrder = "ASC";
        }
    }
    if (index == 2) {
        if (currentComputerSortingMethod == "Type" && currentComputerOrder == "ASC") {
            currentComputers = service.sortComputers("Type", "DESC");
            currentComputerOrder = "DESC";
        }
        else {
            currentComputers = service.sortComputers("Type", "ASC");
            currentComputerSortingMethod = "Type";
            currentComputerOrder = "ASC";
        }
    }
    displayCurrentComputers();
}

void MainWindow::personSectionClickedSlot(int index) {
    if (index == 0) {                                                               // if the name headercolumn was clicked
        if (currentPersonSortingMethod == "Name" && currentPersonOrder == "ASC") {  // if already sorting by name and ascending
            currentPersons = service.sortPersons("Name", "DESC");                   // change the order to descending
            currentPersonOrder = "DESC";
        }
        else {
            currentPersons = service.sortPersons("Name", "ASC");                    // otherwise sort ascending
            currentPersonSortingMethod = "Name";                                    // update the strings to current values
            currentPersonOrder = "ASC";
        }
    }
    if (index == 1) {
        if (currentPersonSortingMethod == "Birthyear" && currentPersonOrder == "ASC") {
            currentPersons = service.sortPersons("Birthyear", "DESC");
            currentPersonOrder = "DESC";
        }
        else {
            currentPersons = service.sortPersons("Birthyear", "ASC");
            currentPersonSortingMethod = "Birthyear";
            currentPersonOrder = "ASC";
        }
    }
    if (index == 2) {
        if (currentPersonSortingMethod == "Deathyear" && currentPersonOrder == "ASC") {
            currentPersons = service.sortPersons("Deathyear", "DESC");
            currentPersonOrder = "DESC";
        }
        else {
            currentPersons = service.sortPersons("Deathyear", "ASC");
            currentPersonSortingMethod = "Deathyear";
            currentPersonOrder = "ASC";
        }
    }
    if (index == 3) {
        if (currentPersonSortingMethod == "Gender" && currentPersonOrder == "ASC") {
            currentPersons = service.sortPersons("Gender", "DESC");
            currentPersonOrder = "DESC";
        }
        else {
            currentPersons = service.sortPersons("Gender", "ASC");
            currentPersonSortingMethod = "Gender";
            currentPersonOrder = "ASC";
        }
    }
    displayCurrentPersons();
}

void MainWindow::on_tabWidget_tabBarClicked(int index) {     // clear errors and reset IDs if the tab is changed
    if (index == 0 || index == 1) {                         // to make sure there are no errors
        personId = 0;
        computerId = 0;
        ui->errorLabel->clear();
    }
}

void MainWindow::updatePersons() {
    currentPersons = service.sortPersons(currentPersonSortingMethod, currentPersonOrder);
}

void MainWindow::updateComputers() {
    currentComputers = service.sortComputers(currentComputerSortingMethod, currentComputerOrder);
}

void MainWindow::setInitialVariables() {
    personId = 0;
    computerId = 0;
    currentPersonSortingMethod = "Name";        // default sortingMethod is name in ascending order
    currentComputerSortingMethod = "Name";
    currentPersonOrder = "ASC";
    currentComputerOrder = "ASC";
    makePersonTable();
    makeComputerTable();
}

void MainWindow::on_personTableWidget_doubleClicked(const QModelIndex& index) {
    ui->errorLabel->clear();
    PersonInfoDialog personInfoDialog;
    personInfoDialog.setPerson(currentPersons[index.row()]);        // prepare dialog with the information from clicked person
    personInfoDialog.exec();
}

void MainWindow::on_computerTableWidget_doubleClicked(const QModelIndex& index) {
    ui->errorLabel->clear();
    ComputerInfoDialog computerInfoDialog;
    computerInfoDialog.setComputer(currentComputers[index.row()]);  // prepare dialog with the information from clicked computer
    computerInfoDialog.exec();
}

void MainWindow::on_computerTableWidget_customContextMenuRequested(const QPoint& pos) {
    QMenu menu;
    menu.addAction(ui->actionAdd);
    menu.addAction(ui->actionRemove);
    menu.addAction(ui->actionConnect);
    menu.addAction(ui->actionInfo);
    menu.exec(ui->computerTableWidget->viewport()->mapToGlobal(pos)); // display right click menu from the cursor point
}

void MainWindow::on_personTableWidget_customContextMenuRequested(const QPoint &pos) {
    QMenu menu;
    menu.addAction(ui->actionAdd);
    menu.addAction(ui->actionRemove);
    menu.addAction(ui->actionConnect);
    menu.addAction(ui->actionInfo);
    menu.exec(ui->personTableWidget->viewport()->mapToGlobal(pos)); // display right click menu from the cursor point
}

void MainWindow::on_actionAdd_triggered() {
    on_addNewButton_clicked();
}

void MainWindow::on_actionRemove_triggered() {
    on_removeButton_clicked();
}

void MainWindow::on_actionConnect_triggered() {
    on_connectButton_clicked();
}

void MainWindow::on_actionInfo_triggered() {
    on_infoButton_clicked();
}


void MainWindow::on_personTableWidget_itemSelectionChanged() {
    ui->errorLabel->clear();                // clear errors if selection in table is changed
}

void MainWindow::on_computerTableWidget_itemSelectionChanged(){
    ui->errorLabel->clear();                // clear errors if selection in table is changed
}

void MainWindow::on_actionAbout_the_authors_triggered(){
    AboutDialog aboutDialog;
    aboutDialog.exec();
}

bool MainWindow::nothingSelected() {
    if (ui->tabWidget->currentIndex() == 0) {                   // if the personTab was selected
        if (!(ui->personTableWidget->selectionModel()->isSelected(ui->personTableWidget->currentIndex()))) {
            ui->errorLabel->setText("Nothing selected");
            return true;
        }
    }
    else if (ui->tabWidget->currentIndex() == 1) {            // if the computerTab was selected
        if (!(ui->computerTableWidget->selectionModel()->isSelected(ui->computerTableWidget->currentIndex()))) {
            ui->errorLabel->setText("Nothing selected");
            return true;
        }
    }
    return false;
}
