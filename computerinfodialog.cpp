#include "computerinfodialog.h"
#include "ui_computerinfodialog.h"

ComputerInfoDialog::ComputerInfoDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ComputerInfoDialog) {
    ui->setupUi(this);
    ui->connectedPersonsTableWidget->setColumnWidth(0, 250);        // make the name column 250 px to fit long names
}

ComputerInfoDialog::~ComputerInfoDialog() {
    delete ui;
}

void ComputerInfoDialog::setComputer(Computer c) {
    this->setWindowTitle(QString::fromStdString(c.getName()));
    ui->nameLabel->setText(QString::fromStdString(c.getName()));
    ui->yearBuiltLabel->setText(QString::fromStdString(c.getYearBuilt()));
    ui->typeLabel->setText(QString::fromStdString(c.getType()));
    QPixmap pixmap(QString::fromStdString(c.getImagePath()));
    ui->photoLabel->setPixmap(pixmap);
    setImage(c.getImagePath());

    vector<Person> connectedPersons = service.getConnectedPersons(c.getId());
    ui->connectedPersonsTableWidget->setRowCount(connectedPersons.size());
    for (unsigned int i = 0; i < connectedPersons.size(); i++) {
        ui->connectedPersonsTableWidget->setItem(i, 0, new QTableWidgetItem(QString::fromStdString(connectedPersons[i].getName())));
        ui->connectedPersonsTableWidget->setItem(i, 1, new QTableWidgetItem(QString::fromStdString(connectedPersons[i].getBirthyear())));
        ui->connectedPersonsTableWidget->setItem(i, 2, new QTableWidgetItem(QString::fromStdString(connectedPersons[i].getDeathyear())));
        ui->connectedPersonsTableWidget->setItem(i, 3, new QTableWidgetItem(QString::fromStdString(connectedPersons[i].getGender())));
    }
}

void ComputerInfoDialog::setImage(const string& imagePath) {
    if (imagePath == "0") {                                                                     // if imagePath = "0"
        QPixmap pixmap(QString::fromStdString(":/Icons/Resources/Noimagecomputer.png"));        // set to default image
        ui->photoLabel->setPixmap(pixmap);
        QPixmap scaledPixMap = pixmap.scaledToHeight(ui->photoLabel->height());
        ui->photoLabel->setPixmap(scaledPixMap);
        ui->photoLabel->setContentsMargins(100, 10, 10, 10);                        // set the margins to center the image
    }
    else {
        QPixmap pixmap(QString::fromStdString(imagePath));                      // otherwise make a pixmap from the imagePath
        ui->photoLabel->setPixmap(pixmap);
        if (pixmap.height() > pixmap.width()) {                                 // check wheather to display in portrait or landscape
            QPixmap scaledPixMap = pixmap.scaledToHeight(ui->photoLabel->height());
            ui->photoLabel->setPixmap(scaledPixMap);
            ui->photoLabel->setContentsMargins(100, 10, 10, 10);                 // set the margins to center the image
        }
        else {
            QPixmap scaledPixMap = pixmap.scaledToWidth(ui->photoLabel->width());
            ui->photoLabel->setPixmap(scaledPixMap);
        }
    }
}
