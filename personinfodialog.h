#ifndef PERSONINFODIALOG_H
#define PERSONINFODIALOG_H

#include <QDialog>
#include <QPixmap>
#include "person.h"
#include "service.h"

namespace Ui {
class PersonInfoDialog;
}

class PersonInfoDialog : public QDialog
{
    Q_OBJECT

public:
    explicit PersonInfoDialog(QWidget *parent = 0);
    ~PersonInfoDialog();
    // receives a person and sets the UI elements with the persons information
    void setPerson(Person p);

private:
    Ui::PersonInfoDialog *ui;
    Service service;
    // checks if the image should be displayed in portrait or landscape and the displays it
    void setImage(const string& imagePath);
};

#endif // PERSONINFODIALOG_H
