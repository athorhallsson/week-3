#ifndef PERSON_H
#define PERSON_H

#include <string>
using namespace std;

class Person {
public:
    Person();
    Person(const string& myName, const string& myBirthYear, const string& myDeathyear, const string& myGender, const string& myImagePath);
    Person(int myId, const string& myName, const string& myBirthYear, const string& myDeathyear, const string& myGender, const string& myImagePath);
    string getName() const;
    string getBirthyear() const;
    string getDeathyear() const;
    string getGender() const;
    string getImagePath() const;
    int getId() const;
private:
    int id;
    string name;
    string birthYear;
    string deathYear;
    string gender;
    string imagePath;
};

#endif // PERSON_H
