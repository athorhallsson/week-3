#ifndef COMPUTERINFODIALOG_H
#define COMPUTERINFODIALOG_H

#include <QDialog>
#include <QPixmap>
#include "Service.h"
#include "person.h"

namespace Ui {
class ComputerInfoDialog;
}

class ComputerInfoDialog : public QDialog {
    Q_OBJECT

public:
    explicit ComputerInfoDialog(QWidget *parent = 0);
    ~ComputerInfoDialog();
    // receives a computer and sets the UI elements with the computers information
    void setComputer(Computer c);

private:
    Ui::ComputerInfoDialog *ui;
    Service service;
    // checks if the image should be displayed in portrait or landscape and the displays it
    void setImage(const string& imagePath);
};

#endif // COMPUTERINFODIALOG_H
