#include "addcomputerdialog.h"
#include "ui_addcomputerdialog.h"

AddComputerDialog::AddComputerDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::AddComputerDialog) {
    ui->setupUi(this);
}

AddComputerDialog::~AddComputerDialog() {
    delete ui;
}

void AddComputerDialog::on_addComputerButton_clicked() {
    string name, yearBuilt, type, imagePath;
    if (validComputer()) {                                              // check if the input is valid
        name = ui->nameLineEdit->text().toStdString();
        bool built = ui->builtCheckBox->checkState();
        if (built) {
            yearBuilt = ui->yearBuiltLineEdit->text().toStdString();
        }
        else {
            yearBuilt = "never built";                 // if the computer was not built set yearBuilt to "never built"
        }
        type = ui->typeLineEdit->text().toStdString();
        if (ui->imageLineEdit->text().isEmpty()) {
            imagePath = "0";                           // set the imagepath to "0" if there was no image selected
        }
        else {
            imagePath = ui->imageLineEdit->text().toStdString();
        }
        service.addComputer(Computer (name, yearBuilt, built, type, imagePath));
        close();
    }
}

void AddComputerDialog::on_builtCheckBox_stateChanged(int checked) {
    if (checked == 0) {
        ui->yearBuiltLineEdit->setStyleSheet("color: gray");    // disable the yearBuilt option if it was not built
        ui->yearBuiltLineEdit->setReadOnly(true);
    }
    if (checked == 2) {
        ui->yearBuiltLineEdit->setStyleSheet("color: black");   // enable the yearBuilt option if it was built
        ui->yearBuiltLineEdit->setReadOnly(false);
    }
}

bool AddComputerDialog::validComputer() {
    bool valid = true;
    int currentYear = QDate::currentDate().year();          // get the current year
    ui->computerNameErrorLabel->clear();
    ui->yearBuiltErrorLabel->clear();
    ui->typeErrorLabel->clear();
    if (ui->nameLineEdit->text().isEmpty()) {
        ui->computerNameErrorLabel->setText("Invalid");
        valid = false;
    }
    if (ui->builtCheckBox->checkState() == 2) {         // if it was built
        if (ui->yearBuiltLineEdit->text().toInt() > currentYear || ui->yearBuiltLineEdit->text().isEmpty()) {
            ui->yearBuiltErrorLabel->setText("Invalid");
            valid = false;
        }
    }
    if (ui->typeLineEdit->text().isEmpty()) {
        ui->typeErrorLabel->setText("Invalid");
        valid = false;
    }
    return valid;
}

void AddComputerDialog::on_browseButton_clicked() {
    QString filename =  QFileDialog::getOpenFileName(this, "Select an image", "../../../","*.jpg *.png *.jpeg");
    ui->imageLineEdit->setText(filename);
}
