#include "aboutdialog.h"
#include "ui_aboutdialog.h"

AboutDialog::AboutDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::AboutDialog) {
    ui->setupUi(this);
    fillInfo();
}

AboutDialog::~AboutDialog() {
    delete ui;
}

void AboutDialog::fillInfo() {
    ui->nameLabel1->setText("Andri Már Þórhallsson");
    ui->nameLabel2->setText("Guðrún Lárusdóttir");
    ui->nameLabel3->setText("Kristófer Ísak Karlsson");
    ui->nameLabel4->setText("Þorsteinn Helgason");
    QPixmap pixmap(QString::fromStdString(":/Icons/Resources/Ada.png"));
    ui->photoLabel->setPixmap(pixmap);
    QPixmap scaledPixMap = pixmap.scaledToHeight(ui->photoLabel->height());
    ui->photoLabel->setPixmap(scaledPixMap);
    ui->photoLabel->setContentsMargins(10, 0, 0, 0);
}
