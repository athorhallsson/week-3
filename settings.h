#ifndef SETTINGS_H
#define SETTINGS_H

#include <QtSql>

const QString DBNAME = "../../../../week3/Resources/week3.sqlite";      // name and path of the database
const QString personConnectionName = "PersonConnection";        // name for connection from PersonRepository
const QString computerConnectionName = "ComputerConnection";    // name for connection from ComputerRepository

#endif // SETTINGS_H
