#include "addpersondialog.h"
#include "ui_addpersondialog.h"

AddPersonDialog::AddPersonDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::AddPersonDialog){
    ui->setupUi(this);
}

AddPersonDialog::~AddPersonDialog() {
    delete ui;
}

void AddPersonDialog::on_addPersonButton_clicked() {
    string name, birthyear, deathyear, gender, imagePath;
    if (validPerson()) {
        name = ui->nameLineEdit->text().toStdString();
        birthyear = ui->birthyearLineEdit-> text().toStdString();
        if (ui->aliveCheckBox->checkState() == 2) {                 // if the person is still alive
            deathyear = "alive";                                    // set deathyear to "alive"
        }
        else {
            deathyear = ui->deathyearLineEdit->text().toStdString();
        }
        gender = ui->genderComboBox->currentText().toStdString();
        if (ui->imageLineEdit->text().isEmpty()) {                  // if no image was selected
            imagePath = "0";                                        // set imagePath="0"
        }
        else {
            imagePath = ui->imageLineEdit->text().toStdString();
        }
        service.addPerson(Person (name, birthyear, deathyear, gender, imagePath));
        close();
    }
}

void AddPersonDialog::on_aliveCheckBox_stateChanged(int checked) {
    if (checked == 0) {
        ui->deathyearLineEdit->setStyleSheet("color: black");       // enable deathyearLineEdit if the person is not alive
        ui->deathyearLineEdit->setReadOnly(false);
    }
    if (checked == 2) {
        ui->deathyearLineEdit->setStyleSheet("color: gray");        // disable deathyearLineEdit if the person is alive
        ui->deathyearLineEdit->setReadOnly(true);
    }
}

bool AddPersonDialog::validPerson() {
    bool valid = true;
    ui->personNameErrorLabel->clear();                              // clear previous errors
    ui->birthyearErrorLabel->clear();
    ui->deathyearErrorLabel->clear();
    int currentYear = QDate::currentDate().year();                  // get current year
    if (ui->nameLineEdit->text().isEmpty()) {
        ui->personNameErrorLabel->setText("Invalid");
        valid = false;
    }
    if (ui->birthyearLineEdit->text().toInt() > currentYear || ui->birthyearLineEdit->text().isEmpty()) {
        ui->birthyearErrorLabel->setText("Invalid");
        valid = false;
    }
    if (ui->aliveCheckBox->checkState() == 0) {
        if (ui->deathyearLineEdit->text().toInt() < ui->birthyearLineEdit->text().toInt() || ui->deathyearLineEdit->text().isEmpty() || ui->deathyearLineEdit->text().toInt() > currentYear) {
            ui->deathyearErrorLabel->setText("Invalid");
            valid = false;
        }
    }
    return valid;
}

void AddPersonDialog::on_browseButton_clicked() {
    QString filename =  QFileDialog::getOpenFileName(this, "Select an image", "../../../","*.jpg *.png *.jpeg");
    ui->imageLineEdit->setText(filename);
}
