#-------------------------------------------------
#
# Project created by QtCreator 2014-12-11T13:11:55
#
#-------------------------------------------------

QT       += core gui
QT       += core sql


greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = week3
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    computer.cpp \
    computerrepository.cpp \
    person.cpp \
    personrepository.cpp \
    service.cpp \
    addcomputerdialog.cpp \
    addpersondialog.cpp \
    personinfodialog.cpp \
    computerinfodialog.cpp \
    aboutdialog.cpp

HEADERS  += mainwindow.h \
    computer.h \
    computerrepository.h \
    person.h \
    personrepository.h \
    service.h \
    settings.h \
    addcomputerdialog.h \
    addpersondialog.h \
    personinfodialog.h \
    computerinfodialog.h \
    aboutdialog.h

FORMS    += mainwindow.ui \
    addcomputerdialog.ui \
    addpersondialog.ui \
    personinfodialog.ui \
    computerinfodialog.ui \
    aboutdialog.ui

RESOURCES = Resources.qrc

macx {
     ICON = ../week3/Resources/Ada.icns         # delete this line if program crashes and also delete the build folder
}


