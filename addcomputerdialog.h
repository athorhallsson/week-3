#ifndef ADDCOMPUTERDIALOG_H
#define ADDCOMPUTERDIALOG_H

#include <QDebug>
#include <QDialog>
#include <QFileDialog>
#include "service.h"
#include "computer.h"

namespace Ui {
class AddComputerDialog;
}

class AddComputerDialog : public QDialog {
    Q_OBJECT

public:
    explicit AddComputerDialog(QWidget *parent = 0);
    ~AddComputerDialog();

private slots:
    void on_addComputerButton_clicked();
    void on_builtCheckBox_stateChanged(int checked);
    void on_browseButton_clicked();

private:
    Service service;
    Ui::AddComputerDialog *ui;
    // checks if the all fields are correctly filled out
    bool validComputer();
};

#endif // ADDCOMPUTERDIALOG_H
