#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QHeaderView>
#include <QMenu>
#include "service.h"
#include "addcomputerdialog.h"
#include "addpersondialog.h"
#include "personinfodialog.h"
#include "computerinfodialog.h"
#include "aboutdialog.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    // executes when text is entered in the search field to search for person/computer
    void on_searchField_textChanged(const QString &searchString);
    // executes when the add button is clicked to add a person/computer
    void on_addNewButton_clicked();
    // executes when the remove button is clicked to remove a person/computer
    void on_removeButton_clicked();
    // executes when the connect button is clicked to connect persons and computers
    void on_connectButton_clicked();
    // executes when the info button is clicked to open the more Info dialog and display connections
    void on_infoButton_clicked();
    // executes when the header in the computerTable is clicked to sort by that column
    void computerSectionClickedSlot(int index);
    // executes when the header in the personTable is clicked to sort by that column
    void personSectionClickedSlot(int index);
    // executes when the tabs are clicked to clear the current message from the errorLabel
    void on_tabWidget_tabBarClicked(int index);
    // executes when a row in the personTable is double clicked to open the more Info dialog and display connections
    void on_personTableWidget_doubleClicked(const QModelIndex &index);
    // executes when a row in the computerTable is double clicked to open the more Info dialog and display connections
    void on_computerTableWidget_doubleClicked(const QModelIndex &index);
    // executes when a row in the personTable is right clicked to display an option to select add, remove, connect or info
    void on_personTableWidget_customContextMenuRequested(const QPoint &pos);
    // executes when a row in the computerTable is right clicked to display an option to select add, remove, connect or info
    void on_computerTableWidget_customContextMenuRequested(const QPoint &pos);
    // actions to display in the customContextMenu (right click)
    void on_actionAdd_triggered();
    void on_actionRemove_triggered();
    void on_actionConnect_triggered();
    void on_actionInfo_triggered();
    // executes when selections in the tables are changed to clear the current message in the errorLabel
    void on_personTableWidget_itemSelectionChanged();
    void on_computerTableWidget_itemSelectionChanged();
    // action in the menu bar to display the application icon and the names of the authors
    void on_actionAbout_the_authors_triggered();

private:
    Ui::MainWindow *ui;
    Service service;
    // vectors that keep the current persons/computers
    vector<Person> currentPersons;
    vector<Computer> currentComputers;
    // initalizes the private variables with the correct values
    void setInitialVariables();
    // fills the table with the items from currentPersons/currentComputers vector
    void displayCurrentComputers();
    void displayCurrentPersons();
    // custom initialization of the tables to override the flawed built-in sorting function
    void makePersonTable();
    void makeComputerTable();
    // updates the vectors with the current sorting method
    void updatePersons();
    void updateComputers();
    // pointers to the headerViews of the tables to override the built in sorting method
    QHeaderView* computerHeaderView;
    QHeaderView* personHeaderView;
    // strings to keep track of the current sorting method
    string currentComputerSortingMethod;
    string currentPersonSortingMethod;
    // strings to keep track of the current order (ascending or descending)
    string currentComputerOrder;
    string currentPersonOrder;
    // integers to keep track of currently selected IDs for use in the connect function
    int personId;
    int computerId;
    // checks if nothing is selected in the tables
    bool nothingSelected();
};

#endif // MAINWINDOW_H
