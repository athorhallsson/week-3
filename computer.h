#ifndef COMPUTER_H
#define COMPUTER_H

#include <string>
using namespace std;

class Computer {
public:
    Computer ();
    Computer (const string& myName, const string& myYearBuilt, bool myBuilt, const string& myType, const string& myImagePath);
    Computer (int myId, const string& myName, const string& myYearBuilt, bool myBuilt, const string& myType, const string& myImagePath);
    string getName() const;
    string getYearBuilt() const;
    bool getBuilt() const;
    string getType() const;
    int getId() const;
    string getImagePath() const;
private:
    int id;
    string name;
    string yearBuilt;
    bool built;
    string type;
    string imagePath;
};

#endif // COMPUTER_H
