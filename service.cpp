#include "Service.h"

Service::Service() {
}

void Service::addComputer(const Computer& c) {
    computerRepo.add(c);
}

void Service::addPerson(const Person& p) {
    personRepo.add(p);
}

vector<Person> Service::findPerson(const string& searchString) {
    return personRepo.find(searchString);
}

vector<Computer> Service::findComputer(const string& searchString) {
    return computerRepo.find(searchString);
}

vector<Person> Service::sortPersons(const string& sortingMethod, const string& order) {
    return personRepo.sort(sortingMethod, order);
}

vector<Computer> Service::sortComputers(const string &sortingMethod, const string &order) {
    return computerRepo.sort(sortingMethod, order);
}

bool Service::connect(int personId, int computerId) {
    return personRepo.connect(personId, computerId);
}

vector<Computer> Service::getConnectedComputers(int personId) {
    return computerRepo.getConnectedComputers(personId);
}

vector<Person> Service::getConnectedPersons(int computerId) {
    return personRepo.getConnectedPersons(computerId);
}

void Service::removePerson(int personId) {
    personRepo.removePerson(personId);
}

void Service::removeComputer(int computerId) {
    computerRepo.removeComputer(computerId);
}
