#include "personinfodialog.h"
#include "ui_personinfodialog.h"

PersonInfoDialog::PersonInfoDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::PersonInfoDialog) {
    ui->setupUi(this);
    ui->connectedComputersTableWidget->setColumnWidth(0, 250);              // set the width of the Name column to 250px
}

PersonInfoDialog::~PersonInfoDialog() {
    delete ui;
}

void PersonInfoDialog::setPerson(Person p) {
    this->setWindowTitle(QString::fromStdString(p.getName()));
    ui->nameLabel->setText(QString::fromStdString(p.getName()));
    ui->birthyearLabel->setText(QString::fromStdString(p.getBirthyear()));
    ui->deathyearLabel->setText(QString::fromStdString(p.getDeathyear()));
    ui->genderLabel->setText(QString::fromStdString(p.getGender()));
    setImage(p.getImagePath());

    vector<Computer> connectedComputers = service.getConnectedComputers(p.getId());     // get the connected computers
    ui->connectedComputersTableWidget->setRowCount(connectedComputers.size());          // put them in the table
    for (unsigned int i = 0; i < connectedComputers.size(); i++) {
        ui->connectedComputersTableWidget->setItem(i, 0, new QTableWidgetItem(QString::fromStdString(connectedComputers[i].getName())));
        ui->connectedComputersTableWidget->setItem(i, 1, new QTableWidgetItem(QString::fromStdString(connectedComputers[i].getYearBuilt())));
        ui->connectedComputersTableWidget->setItem(i, 2, new QTableWidgetItem(QString::fromStdString(connectedComputers[i].getType())));
    }
}

void PersonInfoDialog::setImage(const string& imagePath) {
    if (imagePath == "0") {                                                                     // if imagePath = "0"
        QPixmap pixmap(QString::fromStdString(":/Icons/Resources/Noimageperson.png"));        // set to default image
        ui->photoLabel->setPixmap(pixmap);
        QPixmap scaledPixMap = pixmap.scaledToHeight(ui->photoLabel->height());
        ui->photoLabel->setPixmap(scaledPixMap);
        ui->photoLabel->setContentsMargins(100, 10, 10, 10);                        // set the margins to center the image
    }
    else {
        QPixmap pixmap(QString::fromStdString(imagePath));                      // otherwise make a pixmap from the imagePath
        ui->photoLabel->setPixmap(pixmap);
        if (pixmap.height() > pixmap.width()) {                                 // check wheather to display in portrait or landscape
            QPixmap scaledPixMap = pixmap.scaledToHeight(ui->photoLabel->height());
            ui->photoLabel->setPixmap(scaledPixMap);
            ui->photoLabel->setContentsMargins(100, 10, 10, 10);                 // set the margins to center the image
        }
        else {
            QPixmap scaledPixMap = pixmap.scaledToWidth(ui->photoLabel->width());
            ui->photoLabel->setPixmap(scaledPixMap);
        }
    }
}
